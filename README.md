# Project Report - 108000203
## 2021 Training Course CG HW - OpenGL Robot
## project description
![](https://i.imgur.com/3ZREzsi.jpg )

Using OpenGL to render a robot, which require:
1. only use OpenGL
2. 10 body parts with hierarchy: 
![just like example](https://i.imgur.com/Qn5tUjH.jpg)

4. using texture   
5. animation

*IDE: Visual Studio 2019*

**Demo video:**
{%youtube Xd_P7GRrU2I %}

## How To Use
W/S: forward/backward
A/D: left/right
R/F: up/down
Q/E: left rotate/right rotate
M: play walk animation
I: idle
mouse scroll: head rotate