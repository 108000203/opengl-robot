#version 330

//in vec3 model_color;
in vec2 texCoord;

out vec4 fragColor;

uniform sampler2D diffuseTex;

uniform vec4 ourColor;


void main() {
    // fragColor = vec4(..., 1);
    //fragColor = vec4(1, 1, 0, 1);
    //---------------------------------
    //fragColor = ourColor;
    //fragColor = texture(diffuseTex, texCoord);
   
    fragColor = texture(diffuseTex, texCoord) + vec4(ourColor);
     
    //---------------------------------

}
