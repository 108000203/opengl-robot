﻿#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <vector>
#include<math.h>
//#include <SOIL.h>
#include <glad/glad.h>                                                                                    
#include <GLFW/glfw3.h> // 人機互動control
#include "textfile.h"
#define STB_IMAGE_IMPLEMENTATION
#include <STB/stb_image.h> // to load 圖片

using namespace std;

#define TINYOBJLOADER_IMPLEMENTATION
#include "tiny_obj_loader.h" // tiny object : 原始的完整model object

#define GLM_FORCE_SWIZZLE // Or defined when building (e.g. -DGLM_FORCE_SWIZZLE)
#include <glm/glm.hpp> // 提供現成 rotation translation MVP projection、viewing Matrix
#include "glm/gtc/type_ptr.hpp"

#ifndef max
# define max(a,b) (((a)>(b))?(a):(b))
# define min(a,b) (((a)<(b))?(a):(b))
#endif

#define deg2rad(x) ((x)*((3.1415926f)/(180.0f)))

using namespace glm;
using namespace std;

// Default window size
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;
// current window size
int screenWidth = WINDOW_WIDTH, screenHeight = WINDOW_HEIGHT;

vector<string> filenames; // .obj filename list

int Moving_Mode;
typedef struct
{
	GLuint diffuseTexture; // Texture ID
} PhongMaterial;

typedef struct
{
	GLuint vao;
	GLuint vbo;

	// no use
	GLuint vboTex;
	GLuint ebo;
	GLuint p_color;
	int vertex_count;
	GLuint p_normal;
	GLuint p_texCoord;
	PhongMaterial material;
	int indexCount;
} Shape;
/*
struct model
{
	vec3 position = vec3(0, 0, 0);
	vec3 scale = vec3(1, 1, 1);
	vec3 rotation = vec3(0, 0, 0);	// Euler form

	vector<Shape> shapes;
};
vector<model> models;
*/
struct model
{
	vec3 position = vec3(0, 0, 0);
	vec3 scale = vec3(1, 1, 1);
	vec3 rotation = vec3(0, 0, 0);	// Euler form
	GLfloat rotate_degree = 0;
	GLuint vao;
	GLuint vbo;
};
vector<model> myModels;


struct camera
{
	vec3 position;
	vec3 center;
	vec3 up_vector;
};
camera main_camera;

struct project_setting // projection matrix
{
	GLfloat nearClip, farClip;
	GLfloat fovy;
	GLfloat aspect;
};
project_setting proj;

Shape m_shpae;
// vector<Shape> m_shape_list;

GLuint body_vao;
GLuint head_vao;

int cur_idx = 0; // represent which model should be rendered now
vector<string> model_list{ "../TextureModels/Fushigidane.obj", "../TextureModels/Mew.obj","../TextureModels/Nyarth.obj","../TextureModels/Zenigame.obj", "../TextureModels/texturedknot.obj", "../TextureModels/laurana500.obj", "../TextureModels/Nala.obj" };
//----------------------

//vector<string> mymodel_list{ "../myrobot_1/Robot5_01.obj" }; // not work
//vector<string> mymodel_list{ "../myobj/eyeball.obj" };
//vector<string> mymodel_list{ "../NormalModels/sphereN.obj" };
vector<string> normal_model_list{ "../NormalModels/sphereN.obj" };
//------------------------
GLuint program;

GLuint iLocP;
GLuint iLocV;
GLuint iLocM;
GLuint vertexColorLocation;
GLuint myTexture;
GLuint texID;


mat4 ARM_L1_MOVE();
mat4 ARM_L2_MOVE();
mat4 ARM_R1_MOVE();
mat4 ARM_R2_MOVE();
mat4 LEG_L1_MOVE();
mat4 LEG_L2_MOVE();
mat4 LEG_R1_MOVE();
mat4 LEG_R2_MOVE();

// Call back function for window reshape
void ChangeSize(GLFWwindow* window, int width, int height)
{
	// glViewport(0, 0, width, height);
	proj.aspect = (float)(width) / (float)height;
	screenWidth = width;
	screenHeight = height;

	glViewport(0, 0, screenWidth, screenHeight);
}

// Render function for display rendering
void RenderScene() {
	//printf("///in renerScene///\n");
	// clear canvas
	glClearColor(0.2, 0.2, 0.2, 1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	mat4 T, R, R2, P;
	
	//T = translate(mat4(1.0), myModels[0].position);
	//T = translate(mat4(1.0), glm::vec3(0.0f, 0.0f, 0.0f));
	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(0.0f,  -2.0f, 0.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};
	
	//Build rotation matrix
	GLfloat degree = glfwGetTime() / 1.0; // 10.0
	vec3 rotate_axis = vec3(0.0, 1.0, 0.0);
	//R = rotate(mat4(1.0), degree, rotate_axis);
	//R2 = rotate(mat4(1.0), GLfloat(15.0), vec3(1.0, -1.0, 0.0));

	mat4 project_matrix;
	// perspective(fov, aspect_ratio, near_plane_distance, far_plane_distance)
	// ps. fov = field of view, it represent how much range(degree) is this camera could see 
	project_matrix = perspective(deg2rad(proj.fovy), proj.aspect, proj.nearClip, proj.farClip);

	mat4 view_matrix;
	// lookAt(camera_position, camera_viewing_vector, up_vector)
	// up_vector represent the vector which define the direction of 'up'
	view_matrix = lookAt(main_camera.position, main_camera.center, main_camera.up_vector);
	
	// render object
	//mat4 model_matrix = T * R;
	
	//mat4 model_matrix = R;
	//glUniformMatrix4fv(iLocM, 1, GL_FALSE, value_ptr(model_matrix));
	glUniformMatrix4fv(iLocV, 1, GL_FALSE, value_ptr(view_matrix));
	glUniformMatrix4fv(iLocP, 1, GL_FALSE, value_ptr(project_matrix));

	// my uniform setting
	GLfloat timeValue = glfwGetTime(); 
	//GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
	//GLfloat redValue = (sin(timeValue) / 2) - 0.5;
	//GLfloat blueValue = (cos(timeValue) / 2) + 0.5;
	//glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f); // set color to Uniform in fragment shader 

	/*
	for (int i = 0; i < models[cur_idx].shapes.size(); i++)
	{
		glBindVertexArray(models[cur_idx].shapes[i].vao);
		//-----------------------------------------------------------------------
		glActiveTexture(GL_TEXTURE0); //在绑定纹理之前先激活纹理单元
		glBindTexture(GL_TEXTURE_2D, models[cur_idx].shapes[i].material.diffuseTexture);
		
		//----------------------------------------------------------------------------------
		glDrawArrays(GL_TRIANGLES, 0, models[cur_idx].shapes[i].vertex_count);
		glBindVertexArray(0);
	}
	*/

	// my render
	mat4 HEAD_M = translate(mat4(1.0), vec3(0.0, 1.5, 0.0));

	mat4 ARM_R1 = translate(mat4(1.0), vec3(1.5, 0.5, 0.0));
	mat4 ARM_R2 = translate(mat4(1.0), vec3(0.0, -1.0, 0.0));

	mat4 ARM_L1 = translate(mat4(1.0), vec3(-1.5, 0.5, 0.0));
	mat4 ARM_L2 = translate(mat4(1.0), vec3(0.0, -1.0, 0.0));

	mat4 LEG_R1 = translate(mat4(1.0), vec3(0.5, -1.0, 0.0));
	mat4 LEG_R2 = translate(mat4(1.0), vec3(0.0, -1.5, 0.0));

	mat4 LEG_L1 = translate(mat4(1.0), vec3(-0.5, -1.0, 0.0));
	mat4 LEG_L2 = translate(mat4(1.0), vec3(0.0, -1.5, 0.0));

	mat4 model_matrix;
	mat4 BODY_MODEL_MAT;
	mat4 HEAD_ROTATE_MAT;
	for (GLuint cur_part = 0; cur_part < myModels.size(); cur_part++) {
		glBindVertexArray(myModels[cur_part].vao);

		//T = translate(mat4(1.0), cubePositions[cur_part]);
		P = translate(mat4(1.0), myModels[1].position); // body's position
		
		R = rotate(mat4(1.0), myModels[1].rotate_degree, rotate_axis); // bodyrotation

		BODY_MODEL_MAT = P * R;

		if (cur_part == 0) {
			HEAD_ROTATE_MAT = rotate(mat4(1.0), myModels[0].rotate_degree, rotate_axis); // head rotation
			model_matrix = HEAD_M * BODY_MODEL_MAT * HEAD_ROTATE_MAT;
			GLfloat redValue = (sin(timeValue) / 2) + 0.5;
			glUniform4f(vertexColorLocation, redValue, 0.0f, 0.0f, 1.0f); // set color to Uniform in fragment shader
			
			glActiveTexture(GL_TEXTURE0); //在绑定纹理之前先激活纹理单元
			glBindTexture(GL_TEXTURE_2D, texID);
			glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(3);
			
		}
		else if (cur_part == 1){
			model_matrix = BODY_MODEL_MAT;
			glUniform4f(vertexColorLocation, 0.8f, 0.8f, 0.8f, 1.0f);
			/*
			glActiveTexture(GL_TEXTURE0); //在绑定纹理之前先激活纹理单元
			glBindTexture(GL_TEXTURE_2D, texID);
			glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(3);
			*/

		}
		else if (cur_part == 2) {
			mat4 moving_matrix = ARM_R1_MOVE();
			model_matrix = (P * R) * ARM_R1 * moving_matrix;
			glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (cur_part == 3) {
			mat4 moving_matrix = ARM_L1_MOVE();
			model_matrix = (P * R) * ARM_L1 * moving_matrix;
			//glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f);
			glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f);

		}
		else if (cur_part == 4) {
			mat4 moving_matrix = ARM_R2_MOVE();
			model_matrix = (P * R) * ARM_R1 * ARM_R2 * moving_matrix;
			//glUniform4f(vertexColorLocation, 0.0f, 0.0f, 1.0f, 1.0f);
			glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		else if (cur_part == 5) {
			mat4 moving_matrix = ARM_L2_MOVE();

			model_matrix = (P * R) * ARM_L1 * ARM_L2 * moving_matrix;
			//glUniform4f(vertexColorLocation, 0.0f, 1.0f, 0.0f, 1.0f);
			glUniform4f(vertexColorLocation, 0.0f, 0.0f, 0.0f, 1.0f);
		}
		else if (cur_part == 6) {
			mat4 moving_matrix = LEG_R1_MOVE();
			model_matrix = (P * R) * LEG_R1 * moving_matrix;
			glUniform4f(vertexColorLocation, 0.8f, 0.8f, 0.8f, 1.0f);
		}
		else if (cur_part == 7){
			mat4 moving_matrix = LEG_L1_MOVE();

			model_matrix = (P * R) * LEG_L1 * moving_matrix;
			glUniform4f(vertexColorLocation, 0.8f, 0.8f, 0.8f, 1.0f);
		}
		else if (cur_part == 8) {
			mat4 moving_matrix = LEG_R2_MOVE();

			model_matrix = (P * R) * LEG_R1 * LEG_R2 * moving_matrix;
			glUniform4f(vertexColorLocation, 0.8f, 0.8f, 0.8f, 1.0f);

		}
		else if (cur_part == 9) {
			mat4 moving_matrix = LEG_L2_MOVE();

			model_matrix = (P * R) * LEG_L1 * LEG_L2 * moving_matrix;
			glUniform4f(vertexColorLocation, 0.8f, 0.8f, 0.8f, 1.0f);


		}
		glUniformMatrix4fv(iLocM, 1, GL_FALSE, value_ptr(model_matrix));

		

		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	/*
	T = translate(mat4(1.0), cubePositions[1]);
	mat4 model_matrix = T;
	glUniformMatrix4fv(iLocM, 1, GL_FALSE, value_ptr(model_matrix));

	glBindVertexArray(body_vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);

	model_matrix = translate(mat4(1.0), cubePositions[0]);
	glUniformMatrix4fv(iLocM, 1, GL_FALSE, value_ptr(model_matrix));
	glBindVertexArray(head_vao);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
	*/

	
	
	/*
	for (GLuint i = 0; i < 10; i++) {
		
		glm::mat4 model;
		model = glm::translate(mat4(1.0), cubePositions[i]);
		GLfloat angle = 20.0f * i;
		model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));

		glUniformMatrix4fv(iLocM, 1, GL_FALSE, glm::value_ptr(model));

		glDrawArrays(GL_TRIANGLES, 0, 36);
	}*/

	
	
	//-----------------------------------------------------------------------
	//glActiveTexture(GL_TEXTURE0); //在绑定纹理之前先激活纹理单元
	//glBindTexture(GL_TEXTURE_2D, models[cur_idx].shapes[i].material.diffuseTexture);

	//----------------------------------------------------------------------------------
	//glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}
GLfloat ARM_L1_rotate_degree;
GLfloat ARM_R1_rotate_degree;
GLfloat LEG_L1_rotate_degree;
GLfloat LEG_R1_rotate_degree;


mat4 ARM_L1_MOVE() {
	GLfloat timeValue = glfwGetTime();
	//cout << timeValue << endl;
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		ARM_L1_rotate_degree = GLfloat((sin(timeValue) / 2));
		tmp_matrix = rotate(mat4(1.0), ARM_L1_rotate_degree, vec3(1.0, 0.0, 0.0));
	}
	
	
	return tmp_matrix;
}

mat4 ARM_L2_MOVE() {
	mat4 tmp_matrix = mat4(1.0);
	if (Moving_Mode == 1) {
		if(ARM_L1_rotate_degree >= 0)
			tmp_matrix = rotate(mat4(1.0), 0.0f, vec3(1.0, 0.0, 0.0));
		else 
			tmp_matrix = rotate(mat4(1.0), ARM_L1_rotate_degree*(1.5f), vec3(1.0, 0.0, 0.0));
		tmp_matrix = translate(tmp_matrix, vec3(0.0, 1.0f - 1.0f*cos(ARM_L1_rotate_degree), sin(ARM_L1_rotate_degree) * (-1.0f)));
	}
	
	return tmp_matrix;
}

mat4 ARM_R1_MOVE() {
	GLfloat timeValue = glfwGetTime();
	
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		ARM_R1_rotate_degree = GLfloat((sin(timeValue) / 2)) * GLfloat(-1.0);
		tmp_matrix = rotate(mat4(1.0), ARM_R1_rotate_degree, vec3(1.0, 0.0, 0.0));
	}	
	return tmp_matrix;
}
mat4 ARM_R2_MOVE() {
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		if(ARM_R1_rotate_degree >- 0)
			tmp_matrix = rotate(mat4(1.0), 0.0f, vec3(1.0, 0.0, 0.0));
		else 
			tmp_matrix = rotate(mat4(1.0), ARM_R1_rotate_degree*(1.5f), vec3(1.0, 0.0, 0.0));
		tmp_matrix = translate(tmp_matrix, vec3(0.0, 1.0 - cos(ARM_R1_rotate_degree), sin(ARM_R1_rotate_degree) * (-1.0)));
	}

	return tmp_matrix;
}

mat4 LEG_L1_MOVE() {
	mat4 tmp_matrix = mat4(1.0);

	GLfloat timeValue = glfwGetTime();
	if (Moving_Mode == 1) {
		LEG_L1_rotate_degree = GLfloat((sin(timeValue) / 2)) * GLfloat(-1.0);
		tmp_matrix = rotate(mat4(1.0), LEG_L1_rotate_degree, vec3(1.0, 0.0, 0.0));
	} 
	
	return tmp_matrix;
}
mat4 LEG_L2_MOVE() {
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		if (LEG_L1_rotate_degree <= 0) 
			tmp_matrix = rotate(mat4(1.0), 0.0f, vec3(1.0, 0.0, 0.0));
		else 
			tmp_matrix = rotate(mat4(1.0), LEG_L1_rotate_degree, vec3(1.0, 0.0, 0.0));
		tmp_matrix = translate(tmp_matrix, vec3(0.0, 1.5 - 1.5 * cos(LEG_L1_rotate_degree), sin(LEG_L1_rotate_degree) * (-1.5)));
	}
	return tmp_matrix;
}
mat4 LEG_R1_MOVE() {
	GLfloat timeValue = glfwGetTime();
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		LEG_R1_rotate_degree = GLfloat((sin(timeValue) / 2)) * GLfloat(1.0);
		tmp_matrix = rotate(mat4(1.0), LEG_R1_rotate_degree, vec3(1.0, 0.0, 0.0));
	}

	return tmp_matrix;
}
mat4 LEG_R2_MOVE() {
	mat4 tmp_matrix = mat4(1.0);

	if (Moving_Mode == 1) {
		//cout << LEG_R1_rotate_degree << endl;
		if (LEG_R1_rotate_degree <= 0) {
			//cout << "degub" << endl;
			tmp_matrix = rotate(mat4(1.0), 0.0f, vec3(1.0, 0.0, 0.0));
		}
		else if (LEG_R1_rotate_degree < GLfloat(-0.2)) {
			tmp_matrix = rotate(mat4(1.0), LEG_R1_rotate_degree, vec3(1.0, 0.0, 0.0));
			//cout << "here" << endl;
		}	
		else {
			//cout << "normal" << endl;
			tmp_matrix = rotate(mat4(1.0), LEG_R1_rotate_degree, vec3(1.0, 0.0, 0.0));
		}
			
		tmp_matrix = translate(tmp_matrix, vec3(0.0, 1.5 - 1.5 * cos(LEG_R1_rotate_degree), sin(LEG_R1_rotate_degree) * (-1.5)));
	}
	return tmp_matrix;
}

// Call back function for keyboard
void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{	
	if (action == GLFW_PRESS) {
		switch (key)
		{
		case GLFW_KEY_D:
			printf("press d!\n");
			//myModels[0].position.x += 1;
			myModels[1].position.x += 1;
			/*
			myModels[2].position.x += 1;
			myModels[3].position.x += 1;
			myModels[4].position.x += 1;
			myModels[5].position.x += 1;
			myModels[6].position.x += 1;
			myModels[7].position.x += 1;
			myModels[8].position.x += 1;
			myModels[9].position.x += 1;
			*/
			break;
		case GLFW_KEY_A:
			printf("press a!\n");
			//myModels[0].position.x -= 1;
			myModels[1].position.x -= 1;
			/*
			myModels[2].position.x -= 1;
			myModels[3].position.x -= 1;
			myModels[4].position.x -= 1;
			myModels[5].position.x -= 1;
			myModels[6].position.x -= 1;
			myModels[7].position.x -= 1;
			myModels[8].position.x -= 1;
			myModels[9].position.x -= 1;
			*/
			break;
		case GLFW_KEY_W:
			printf("press w!\n");
			//myModels[0].position.z -= 1;
			myModels[1].position.z -= 1;
			//myModels[2].position.z -= 1;
			//myModels[3].position.z -= 1;
			//myModels[4].position.z -= 1;
			//myModels[5].position.z -= 1;
			//myModels[6].position.z -= 1;
			//myModels[7].position.z -= 1;
			//myModels[8].position.z -= 1;
			//myModels[9].position.z -= 1;
			break;
		case GLFW_KEY_S:
			printf("press s!\n");
			//myModels[0].position.z += 1;
			myModels[1].position.z += 1;
			/*
			myModels[2].position.z += 1;
			myModels[3].position.z += 1;
			myModels[4].position.z += 1;
			myModels[5].position.z += 1;
			myModels[6].position.z += 1;
			myModels[7].position.z += 1;
			myModels[8].position.z += 1;
			myModels[9].position.z += 1;
			*/
			break;
		case  GLFW_KEY_R:
			printf("press r!\n");
			//myModels[0].position.y += 1;
			myModels[1].position.y += 1;
			/*
			myModels[2].position.y += 1;
			myModels[3].position.y += 1;
			myModels[4].position.y += 1;
			myModels[5].position.y += 1;
			myModels[6].position.y += 1;
			myModels[7].position.y += 1;
			myModels[8].position.y += 1;
			myModels[9].position.y += 1;
			*/
			break;
		case  GLFW_KEY_F:
			printf("press f!\n");
			//myModels[0].position.y -= 1;
			myModels[1].position.y -= 1;
			/*
			myModels[2].position.y -= 1;
			myModels[3].position.y -= 1;
			myModels[4].position.y -= 1;
			myModels[5].position.y -= 1;
			myModels[6].position.y -= 1;
			myModels[7].position.y -= 1;
			myModels[8].position.y -= 1;
			myModels[9].position.y -= 1;
			*/
			break;
		case  GLFW_KEY_Q:
			printf("press q!\n");
			myModels[1].rotate_degree -= 1.0;
			cout << myModels[1].rotate_degree << endl;
			break;
		case  GLFW_KEY_E:
			printf("press e!\n");
			myModels[1].rotate_degree += 1.0;
			break;
		case  GLFW_KEY_M:
			printf("press m!\n");
			Moving_Mode = 1;
			break;

		case  GLFW_KEY_I:
			printf("press i!\n");
			Moving_Mode = 0;
			break;
		default:
			break;
		}
	}
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	// scroll up positive, otherwise it would be negtive
	if (yoffset == 1) {
		myModels[cur_idx].rotate_degree += 3;
	} else if  (yoffset == -1) {
		myModels[cur_idx].rotate_degree -= 3;
	} else {
		myModels[cur_idx].rotate_degree = 0;
	}
	printf("Scroll Event: (%f, %f)\n", xoffset, yoffset);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
		printf("Click Mouse Left button!\n");
	else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
		printf("Release Mouse Left button!\n");
	}
}

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
	// callback rigistrate on mouse position
	//printf("cursor position: (%f, %f)\n", xpos, ypos);
}

void setShaders()
{
	GLuint v, f, p;
	char *vs = NULL;
	char *fs = NULL;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	vs = textFileRead("shader.vs.glsl");
	fs = textFileRead("shader.fs.glsl");

	glShaderSource(v, 1, (const GLchar**)&vs, NULL);
	glShaderSource(f, 1, (const GLchar**)&fs, NULL);

	free(vs);
	free(fs);

	GLint success;
	char infoLog[1000];
	// compile vertex shader
	glCompileShader(v);
	// check for shader compile errors
	glGetShaderiv(v, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(v, 1000, NULL, infoLog);
		std::cout << "ERROR: VERTEX SHADER COMPILATION FAILED\n" << infoLog << std::endl;
	}

	// compile fragment shader
	glCompileShader(f);
	// check for shader compile errors
	glGetShaderiv(f, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(f, 1000, NULL, infoLog);
		std::cout << "ERROR: FRAGMENT SHADER COMPILATION FAILED\n" << infoLog << std::endl;
	}

	// create program object
	p = glCreateProgram();

	// attach shaders to program object
	glAttachShader(p,f);
	glAttachShader(p,v);

	// link program
	glLinkProgram(p);
	// check for linking errors
	glGetProgramiv(p, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(p, 1000, NULL, infoLog);
		std::cout << "ERROR: SHADER PROGRAM LINKING FAILED\n" << infoLog << std::endl;
	}

	glDeleteShader(v);
	glDeleteShader(f);

	if (success)
		glUseProgram(p);
    else
    {
        system("pause");
        exit(123);
    }

	program = p;
}

void normalization(tinyobj::attrib_t* attrib, vector<GLfloat>& vertices, vector<GLfloat>& colors, vector<GLfloat>& normals, vector<GLfloat>& textureCoords, vector<int>& material_id, tinyobj::shape_t* shape)
{
	vector<float> xVector, yVector, zVector;
	float minX = 10000, maxX = -10000, minY = 10000, maxY = -10000, minZ = 10000, maxZ = -10000;

	// find out min and max value of X, Y and Z axis
	for (int i = 0; i < attrib->vertices.size(); i++)
	{
		//maxs = max(maxs, attrib->vertices.at(i));
		if (i % 3 == 0)
		{

			xVector.push_back(attrib->vertices.at(i));

			if (attrib->vertices.at(i) < minX)
			{
				minX = attrib->vertices.at(i);
			}

			if (attrib->vertices.at(i) > maxX)
			{
				maxX = attrib->vertices.at(i);
			}
		}
		else if (i % 3 == 1)
		{
			yVector.push_back(attrib->vertices.at(i));

			if (attrib->vertices.at(i) < minY)
			{
				minY = attrib->vertices.at(i);
			}

			if (attrib->vertices.at(i) > maxY)
			{
				maxY = attrib->vertices.at(i);
			}
		}
		else if (i % 3 == 2)
		{
			zVector.push_back(attrib->vertices.at(i));

			if (attrib->vertices.at(i) < minZ)
			{
				minZ = attrib->vertices.at(i);
			}

			if (attrib->vertices.at(i) > maxZ)
			{
				maxZ = attrib->vertices.at(i);
			}
		}
	}
	
	float offsetX = (maxX + minX) / 2;
	float offsetY = (maxY + minY) / 2;
	float offsetZ = (maxZ + minZ) / 2;

	for (int i = 0; i < attrib->vertices.size(); i++)
	{
		if (offsetX != 0 && i % 3 == 0)
		{
			attrib->vertices.at(i) = attrib->vertices.at(i) - offsetX;
		}
		else if (offsetY != 0 && i % 3 == 1)
		{
			attrib->vertices.at(i) = attrib->vertices.at(i) - offsetY;
		}
		else if (offsetZ != 0 && i % 3 == 2)
		{
			attrib->vertices.at(i) = attrib->vertices.at(i) - offsetZ;
		}
	}

	float greatestAxis = maxX - minX;
	float distanceOfYAxis = maxY - minY;
	float distanceOfZAxis = maxZ - minZ;

	if (distanceOfYAxis > greatestAxis)
	{
		greatestAxis = distanceOfYAxis;
	}

	if (distanceOfZAxis > greatestAxis)
	{
		greatestAxis = distanceOfZAxis;
	}

	float scale = greatestAxis / 2;

	for (int i = 0; i < attrib->vertices.size(); i++)
	{
		//std::cout << i << " = " << (double)(attrib.vertices.at(i) / greatestAxis) << std::endl;
		attrib->vertices.at(i) = attrib->vertices.at(i)/ scale;
	}
	size_t index_offset = 0;
	for (size_t f = 0; f < shape->mesh.num_face_vertices.size(); f++) {
		int fv = shape->mesh.num_face_vertices[f];

		// Loop over vertices in the face.
		for (size_t v = 0; v < fv; v++) {
			// access to vertex
			tinyobj::index_t idx = shape->mesh.indices[index_offset + v];
			vertices.push_back(attrib->vertices[3 * idx.vertex_index + 0]);
			vertices.push_back(attrib->vertices[3 * idx.vertex_index + 1]);
			vertices.push_back(attrib->vertices[3 * idx.vertex_index + 2]);
			// Optional: vertex colors
			colors.push_back(attrib->colors[3 * idx.vertex_index + 0]);
			colors.push_back(attrib->colors[3 * idx.vertex_index + 1]);
			colors.push_back(attrib->colors[3 * idx.vertex_index + 2]);
			// Optional: vertex normals
			normals.push_back(attrib->normals[3 * idx.normal_index + 0]);
			normals.push_back(attrib->normals[3 * idx.normal_index + 1]);
			normals.push_back(attrib->normals[3 * idx.normal_index + 2]);
			// Optional: texture coordinate
			textureCoords.push_back(attrib->texcoords[2 * idx.texcoord_index + 0]);
			textureCoords.push_back(attrib->texcoords[2 * idx.texcoord_index + 1]);
			// The material of this vertex
			material_id.push_back(shape->mesh.material_ids[f]);
		}
		index_offset += fv;
	}
}

static string GetBaseDir(const string& filepath) {
	if (filepath.find_last_of("/\\") != std::string::npos)
		return filepath.substr(0, filepath.find_last_of("/\\"));
	return "";
}

GLuint LoadTextureImage(string image_path)
{
	std::cout << image_path << std::endl;
	printf("/// in loadtestureImage ///\n");
	int channel, width, height;
	int require_channel = 4;
	printf("debug here0\n");
	stbi_set_flip_vertically_on_load(true);
	printf("debug here\n");
	stbi_uc *data = stbi_load(image_path.c_str(), &width, &height, &channel, require_channel);
	
	printf("debug here 1\n");
	if (data != NULL)
	{
		GLuint tex; // texture ID
		printf("debug here 2\n");
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		 
		// zoom-in zoom-out setting
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		//------------------------------
		//------------------------------
		stbi_image_free(data);
		return tex;
	}
	else
	{
		cout << "LoadTextureImage: Cannot load image from " << image_path << endl;
		return -1;
	}
}

vector<Shape> SplitShapeByMaterial(vector<GLfloat>& vertices, vector<GLfloat>& colors, vector<GLfloat>& normals, vector<GLfloat>& textureCoords, vector<int>& material_id, vector<PhongMaterial>& materials)
{
	vector<Shape> res;
	for (int m = 0; m < materials.size(); m++)
	{
		vector<GLfloat> m_vertices, m_colors, m_normals, m_textureCoords;
		for (int v = 0; v < material_id.size(); v++) 
		{
			// extract all vertices with same material id and create a new shape for it.
			if (material_id[v] == m)
			{
				m_vertices.push_back(vertices[v * 3 + 0]);
				m_vertices.push_back(vertices[v * 3 + 1]);
				m_vertices.push_back(vertices[v * 3 + 2]);

				m_colors.push_back(colors[v * 3 + 0]);
				m_colors.push_back(colors[v * 3 + 1]);
				m_colors.push_back(colors[v * 3 + 2]);

				m_normals.push_back(normals[v * 3 + 0]);
				m_normals.push_back(normals[v * 3 + 1]);
				m_normals.push_back(normals[v * 3 + 2]);

				m_textureCoords.push_back(textureCoords[v * 2 + 0]);
				m_textureCoords.push_back(textureCoords[v * 2 + 1]);
			}
		}

		if (!m_vertices.empty())
		{
			Shape tmp_shape;
			glGenVertexArrays(1, &tmp_shape.vao);
			glBindVertexArray(tmp_shape.vao);

			glGenBuffers(1, &tmp_shape.vbo);
			glBindBuffer(GL_ARRAY_BUFFER, tmp_shape.vbo);
			glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(GL_FLOAT), &m_vertices.at(0), GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
			tmp_shape.vertex_count = m_vertices.size() / 3;

			glGenBuffers(1, &tmp_shape.p_color);
			glBindBuffer(GL_ARRAY_BUFFER, tmp_shape.p_color);
			glBufferData(GL_ARRAY_BUFFER, m_colors.size() * sizeof(GL_FLOAT), &m_colors.at(0), GL_STATIC_DRAW);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glGenBuffers(1, &tmp_shape.p_normal);
			glBindBuffer(GL_ARRAY_BUFFER, tmp_shape.p_normal);
			glBufferData(GL_ARRAY_BUFFER, m_normals.size() * sizeof(GL_FLOAT), &m_normals.at(0), GL_STATIC_DRAW);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glGenBuffers(1, &tmp_shape.p_texCoord);
			glBindBuffer(GL_ARRAY_BUFFER, tmp_shape.p_texCoord);
			glBufferData(GL_ARRAY_BUFFER, m_textureCoords.size() * sizeof(GL_FLOAT), &m_textureCoords.at(0), GL_STATIC_DRAW);
			glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glEnableVertexAttribArray(2);
			glEnableVertexAttribArray(3);

			tmp_shape.material = materials[m];
			res.push_back(tmp_shape);
		}
	}

	return res;
}
/*
void LoadTexturedModels(string model_path)
{
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	tinyobj::attrib_t attrib;
	vector<GLfloat> vertices;
	vector<GLfloat> colors;
	vector<GLfloat> normals;
	vector<GLfloat> textureCoords;
	vector<int> material_id;

	string err;
	string warn;

	string base_dir = GetBaseDir(model_path); // handle .mtl with relative path

#ifdef _WIN32
	base_dir += "\\";
#else
	base_dir += "/";
#endif
	// use tinyobj library to load .obj file 
	// OBJ模型檔的固定的參數行式
	std::cout << "base_dir: " << base_dir << std::endl;
	std::cout << "model_path: " << model_path << std::endl;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, model_path.c_str(), base_dir.c_str());

	if (!warn.empty()) {
		cout << warn << std::endl;
	}

	if (!err.empty()) {
		cerr << err << std::endl;
	}

	if (!ret) {
		exit(1);
	}

	printf("Load Models Success ! Shapes size %d Material size %d\n", shapes.size(), materials.size());
	model tmp_model;

	vector<PhongMaterial> allMaterial;
	for (int i = 0; i < materials.size(); i++)
	{
		
		PhongMaterial material;
		material.diffuseTexture = LoadTextureImage(base_dir + string(materials[i].diffuse_texname)); // return gen text ID
		if (material.diffuseTexture == -1)
		{
			cout << "LoadTexturedModels: Fail to load model's material " << i << endl;
			system("pause");
			
		}
		
		allMaterial.push_back(material);
	}
	// 將讀到的所以屬性存入shape這個object中
	for (int i = 0; i < shapes.size(); i++)
	{

		vertices.clear();
		colors.clear();
		normals.clear();
		textureCoords.clear();
		material_id.clear();
		normalization(&attrib, vertices, colors, normals, textureCoords, material_id, &shapes[i]);
		// printf("Vertices size: %d", vertices.size() / 3);

		// split current shape into multiple shapes base on material_id.
		vector<Shape> splitedShapeByMaterial = SplitShapeByMaterial(vertices, colors, normals, textureCoords, material_id, allMaterial);

		// concatenate splited shape to model's shape list
		tmp_model.shapes.insert(tmp_model.shapes.end(), splitedShapeByMaterial.begin(), splitedShapeByMaterial.end());
	}
	shapes.clear();
	materials.clear();
	models.push_back(tmp_model);
}

void LoadNormalModels(string model_path) {

	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	tinyobj::attrib_t attrib;
	vector<GLfloat> vertices;
	vector<GLfloat> colors;
	vector<GLfloat> normals;
	vector<GLfloat> textureCoords;
	vector<int> material_id;

	string err;
	string warn;

	string base_dir = GetBaseDir(model_path); // handle .mtl with relative path

#ifdef _WIN32
	base_dir += "\\";
#else
	base_dir += "/";
#endif
	// use tinyobj library to load .obj file 
	// OBJ模型檔的固定的參數行式
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, model_path.c_str(), base_dir.c_str());

	if (!warn.empty()) {
		cout << warn << std::endl;
	}

	if (!err.empty()) {
		cerr << err << std::endl;
	}

	if (!ret) {
		exit(1);
	}

	//printf("Load Models Success ! Shapes size %d Material size %d\n", shapes.size(), materials.size());
	model tmp_model;
	
	vector<PhongMaterial> allMaterial;
	for (int i = 0; i < materials.size(); i++)
	{

		PhongMaterial material;
		material.diffuseTexture = LoadTextureImage(base_dir + string(materials[i].diffuse_texname)); // return gen text ID
		if (material.diffuseTexture == -1)
		{
			cout << "LoadTexturedModels: Fail to load model's material " << i << endl;
			system("pause");

		}

		allMaterial.push_back(material);
	}
	
	// 將讀到的所以屬性存入shape這個object中
	for (int i = 0; i < shapes.size(); i++)
	{

		vertices.clear();
		colors.clear();
		normals.clear();
		textureCoords.clear();
		material_id.clear();
		normalization(&attrib, vertices, colors, normals, textureCoords, material_id, &shapes[i]);
		// printf("Vertices size: %d", vertices.size() / 3);

		// split current shape into multiple shapes base on material_id.
		vector<Shape> splitedShapeByMaterial = SplitShapeByMaterial(vertices, colors, normals, textureCoords, material_id, allMaterial);

		// concatenate splited shape to model's shape list
		tmp_model.shapes.insert(tmp_model.shapes.end(), splitedShapeByMaterial.begin(), splitedShapeByMaterial.end());
	}
	shapes.clear();
	materials.clear();
	models.push_back(tmp_model);
}
*/
void initParameter()
{
	proj.nearClip = 0.001;
	proj.farClip = 1000.0;
	proj.fovy = 80;
	proj.aspect = (float)(WINDOW_WIDTH) / (float)WINDOW_HEIGHT; // adjust width for side by side view

	main_camera.position = vec3(0.0f, 0.0f, 2.0f); // viewing matrix
	main_camera.center = vec3(0.0f, 0.0f, 0.0f);
	main_camera.up_vector = vec3(0.0f, 1.0f, 0.0f);
}

void setUniformVariables() // ???
{
	iLocP = glGetUniformLocation(program, "um4p");
	iLocV = glGetUniformLocation(program, "um4v");
	iLocM = glGetUniformLocation(program, "um4m");
	//
	//GLfloat timeValue = glfwGetTime();
	//GLfloat greenValue = (sin(timeValue) / 2) + 0.5;
	vertexColorLocation = glGetUniformLocation(program, "ourColor");
}

/*
void setupRC()
{
	// setup shaders
	setShaders(); // read and compile
	initParameter();
	setUniformVariables(); // read uniform into shader

	// OpenGL States and Values
	glClearColor(0.2, 0.2, 0.2, 1.0);
	
	for (string model_path : model_list) {
		LoadTexturedModels(model_path);
	}
	
}
*/
void mySetupRC()
{
	// setup shaders
	setShaders(); // read and compile
	initParameter();
	setUniformVariables(); // read uniform into shader

	// OpenGL States and Values
	glClearColor(0.2, 0.2, 0.2, 1.0);

	// Use as body cube
	GLfloat arm_vertices_0[] = {
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	 0.5f,  1.0f, -0.5f,  1.0f, 1.0f, // y*2
	 0.5f,  1.0f, -0.5f,  1.0f, 1.0f, // y*2
	-0.5f,  1.0f, -0.5f,  0.0f, 1.0f, // y*2
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  1.0f,  0.5f,  1.0f, 1.0f,
	 0.5f,  1.0f,  0.5f,  1.0f, 1.0f,
	-0.5f,  1.0f,  0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  1.0f,  0.5f,  1.0f, 0.0f,
	-0.5f,  1.0f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  1.0f,  0.5f,  1.0f, 0.0f,

	 0.5f,  1.0f,  0.5f,  1.0f, 0.0f,
	 0.5f,  1.0f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f,  1.0f,  0.5f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  1.0f, -0.5f,  0.0f, 1.0f,
	 0.5f,  1.0f, -0.5f,  1.0f, 1.0f,
	 0.5f,  1.0f,  0.5f,  1.0f, 0.0f,
	 0.5f,  1.0f,  0.5f,  1.0f, 0.0f,
	-0.5f,  1.0f,  0.5f,  0.0f, 0.0f,
	-0.5f,  1.0f, -0.5f,  0.0f, 1.0f
	};
	GLfloat arm_vertices[] = {
	-0.5f, -1.0f, -0.5f,  0.0f, 0.0f,
	 0.5f, -1.0f, -0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f, // y*2
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f, // y*2
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, // y*2
	-0.5f, -1.0f, -0.5f,  0.0f, 0.0f,

	-0.5f, -1.0f,  0.5f,  0.0f, 0.0f,
	 0.5f, -1.0f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -1.0f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -1.0f, -0.5f,  0.0f, 1.0f,
	-0.5f, -1.0f, -0.5f,  0.0f, 1.0f,
	-0.5f, -1.0f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -1.0f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.0f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.0f,  0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	-0.5f, -1.0f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.0f, -0.5f,  1.0f, 1.0f,
	 0.5f, -1.0f,  0.5f,  1.0f, 0.0f,
	 0.5f, -1.0f,  0.5f,  1.0f, 0.0f,
	-0.5f, -1.0f,  0.5f,  0.0f, 0.0f,
	-0.5f, -1.0f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};

	GLfloat leg_vertices[] = {
	-0.5f, -1.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -1.5f, -0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f, // y*2
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f, // y*2
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, // y*2
	-0.5f, -1.5f, -0.5f,  0.0f, 0.0f,

	-0.5f, -1.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -1.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -1.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -1.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -1.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -1.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -1.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.5f,  0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	-0.5f, -1.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -1.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -1.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -1.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -1.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -1.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};

	GLfloat head_vertices[] = {
	-0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  0.0f, 1.0f,// 後下
	 0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 

	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f, //前
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, // 左
	-0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};
GLfloat body_vertices[] = {
	-1.0f,-1.0f,-1.0f, 0.0f, 0.0f, // triangle 1 : begin
	-1.0f,-1.0f, 1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f, 1.0f, 1.0f, 1.0f, // triangle 1 : end

	1.0f, 1.0f, -1.0f, 0.0f, 1.0f,// triangle 2 : begin
	-1.0f,-1.0f,-1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f,-1.0f, 1.0f, 1.0f, // triangle 2 : end

	1.0f,-1.0f, 1.0f, 0.0f, 0.0f,
	-1.0f,-1.0f,-1.0f, 1.0f, 1.0f,
	1.0f,-1.0f,-1.0f, 0.0f, 1.0f,

	1.0f, 1.0f,-1.0f, 0.0f, 1.0f,
	1.0f,-1.0f,-1.0f, 0.0f, 0.0f,
	-1.0f,-1.0f,-1.0f, 1.0f, 0.0f,

	-1.0f,-1.0f,-1.0f, 0.0f, 0.0f,
	-1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f, 0.0f, 1.0f,

	1.0f,-1.0f, 1.0f, 0.0f, 0.0f,
	-1.0f,-1.0f, 1.0f, 1.0f, 0.0f,
	-1.0f,-1.0f,-1.0f, 1.0f, 1.0f,

	-1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	-1.0f,-1.0f, 1.0f, 0.0f, 0.0f,
	1.0f,-1.0f, 1.0f, 1.0f, 0.0f,

	1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	1.0f,-1.0f,-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f,-1.0f, 1.0f, 1.0f,

	1.0f,-1.0f,-1.0f, 1.0f, 0.0f,
	1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	1.0f,-1.0f, 1.0f, 0.0f, 0.0f,

	1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	1.0f, 1.0f,-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f,-1.0f, 0.0f, 1.0f,

	1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
	-1.0f, 1.0f,-1.0f, 0.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, 0.0f, 0.0f,

	1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, 0.0f, 1.0f,
	1.0f,-1.0f, 1.0f, 1.0f, 0.0f,
	};

	// set #1 Cube model
	GLuint NumberOfPart = 10; // 1. head // 0.body
	
	for (GLuint i = 0; i < NumberOfPart; i++) {

		model tmp_model;
		glGenVertexArrays(1, &tmp_model.vao);
		glBindVertexArray(tmp_model.vao);
		/*
		int width, height;
		unsigned char* image = SOIL_load_image("container.jpg", &width, &height, 0, SOIL_LOAD_RGB);
		*/
		/*
		glGenTextures(1, &myTexture);
		glBindTexture(GL_TEXTURE_2D, myTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		*/
		texID = LoadTextureImage("../myobj/awesomeface.png");
		//GLuint vbo;
		glGenBuffers(1, &tmp_model.vbo);
		glBindBuffer(GL_ARRAY_BUFFER, tmp_model.vbo);
		if (i == 0) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(head_vertices), head_vertices, GL_STATIC_DRAW);

		}
		else if (i == 1) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(body_vertices), body_vertices, GL_STATIC_DRAW);

		}
		else if (i == 2 || i == 3 || i == 4 || i == 5 ) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(arm_vertices), arm_vertices, GL_STATIC_DRAW);
		}
		else if (i == 6 || i == 7) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(leg_vertices), leg_vertices, GL_STATIC_DRAW);
		}
		else if (i == 8 || i == 9) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(leg_vertices), leg_vertices, GL_STATIC_DRAW);
		}
		
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		
		myModels.push_back(tmp_model);
		printf("my model size: %d\n", myModels.size());


		glBindVertexArray(0);
	}
	
	/*
	glGenVertexArrays(1, &body_vao);
	glBindVertexArray(body_vao)
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(body_vertices), body_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	
	//tmp_shape.vertex_count = m_vertices.size() / 3;
	*/
	/*
	glGenVertexArrays(1, &body_vao);
	glBindVertexArray(body_vao);

	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(body_vertices), body_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	
	
	glGenVertexArrays(1, &head_vao);
	glBindVertexArray(head_vao);

	GLuint vbo2;
	glGenBuffers(1, &vbo2);
	glBindBuffer(GL_ARRAY_BUFFER, vbo2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(body_vertices), body_vertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
	
	*/

	//glEnableVertexAttribArray(0);
	//glEnableVertexAttribArray(1);
	//glEnableVertexAttribArray(2);
	//glEnableVertexAttribArray(3);
	
	
}

void glPrintContextInfo(bool printExtension)
{
	cout << "GL_VENDOR = " << (const char*)glGetString(GL_VENDOR) << endl;
	cout << "GL_RENDERER = " << (const char*)glGetString(GL_RENDERER) << endl;
	cout << "GL_VERSION = " << (const char*)glGetString(GL_VERSION) << endl;
	cout << "GL_SHADING_LANGUAGE_VERSION = " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
	if (printExtension)
	{
		GLint numExt;
		glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
		cout << "GL_EXTENSIONS =" << endl;
		for (GLint i = 0; i < numExt; i++)
		{
			cout << "\t" << (const char*)glGetStringi(GL_EXTENSIONS, i) << endl;
		}
	}
}


int main(int argc, char **argv)
{

    // initial glfw
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // fix compilation on OS X
#endif

    
    // create window
	GLFWwindow* window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "108000203 CG Training Coarse", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    
    // load OpenGL function pointer
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

	glPrintContextInfo(false);
    
	// register glfw callback functions
    glfwSetKeyCallback(window, KeyCallback); // aready defined 
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_pos_callback);
    glfwSetFramebufferSizeCallback(window, ChangeSize);

	glEnable(GL_DEPTH_TEST);
	// Setup render context
	//setupRC();
	mySetupRC();
	printf("done mysetupRC\n");
	// main loop
    while (!glfwWindowShouldClose(window))
    {
        // Render 
		// calling drawcall
        RenderScene();
		//printf("1");
        // swap buffer from back to front
        glfwSwapBuffers(window);
        
        // Poll input event
        glfwPollEvents();
    }
	
	// just for compatibiliy purposes
	return 0;
}
